import React from 'react'
import Card from 'react-bootstrap/Card';
import { ListGroup } from 'react-bootstrap';
import LOGO from '../logo.svg';

const UserCard = ({myJSON, loading}) => {
  // Display loading message if data is not loaded yet
  if (loading && !myJSON.name) {

    return (
      <Card className="text-center" id="card" style={{ width: "22rem"}}>
        <Card.Header>
          <Card.Img variant="top" src={LOGO} alt="Logo"/>
        </Card.Header>
        <Card.Body>
          <Card.Title>My User Card</Card.Title>
            <Card.Text >
              Loading....
            </Card.Text>
        </Card.Body>
      </Card>
    )
    
  } else {
    return (
      <Card className="text-center" id="card" style={{ width: "22rem"}}>
        <Card.Header>
          <Card.Img variant="top" src={myJSON.avatar_url} alt="Logo"/>
        </Card.Header>
        <Card.Body>
          <Card.Title>My User Card</Card.Title>
            <Card.Text >
              This is the information extracted from my GitLab account!
            </Card.Text>
            <ListGroup>
                <ListGroup.Item className="border border-dark"><b>NAME:</b> {myJSON.name}</ListGroup.Item>
                <ListGroup.Item className="border border-dark"><b>EMAIL:</b> {myJSON.email}</ListGroup.Item>
                <ListGroup.Item className="border border-dark"><b>USERNAME:</b> {myJSON.username}</ListGroup.Item>
            </ListGroup>
        </Card.Body>
      </Card>
    )
    
  }
};

export default UserCard;