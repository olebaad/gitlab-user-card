import React from 'react';
import Card from './Components/UserCard.js';
import './App.css';
import { withRouter } from 'react-router-dom'

require('dotenv').config();

class App extends React.Component {
  state = {
    myJSON: {},
    client_id: process.env.REACT_APP_CLIENT_ID,
    loading: true,
    access_token: "",
    token_type: "",
  }

  componentDidMount() {
    this.asyncLoad()
    .then(() => {
      if (this.state.access_token.length) {
        let uri = 'https://gitlab.com/api/v4/user';
        let h = new Headers();
        h.append('Authorization', `${this.state.token_type} ${this.state.access_token}`);
    
        let req = new Request(uri, {
          method: 'GET',
          headers: h,
        });

        fetch(req)
        .then((response) => { 
          return response.json();
        })
        .then( data => {
          this.setState({ myJSON: data });
          this.setState({ loading: false });
        })
        .catch(err => console.log('ERROR', err))
      } else {
        return console.log('no access yet');
      }
    })
  }

  async asyncLoad () {

    const { location } = this.props
    
    if (location.hash) {
      const searchParameters = [...new URLSearchParams(location.hash.substring(1)).entries()]
      searchParameters.forEach((item) => {
        if(item[0] === 'access_token') {
          this.setState( { access_token: item[1] })
        } else if (item[0] === 'token_type') {
          this.setState( { token_type: item[1] })
        }
      })
      return;
    }
    
    let redirection = '';
    if (process.env.NODE_ENV === 'development') {
      redirection = process.env.REACT_APP_REDIRECT_URI_DEV;
    } else {
      redirection = process.env.REACT_APP_REDIRECT_URI_PRODUCTION;
    }
    const searchParam = new URLSearchParams({
      redirect_uri: redirection,
      client_id: this.state.client_id,
      response_type: 'token',
    }).toString();

    const uri = `https://gitlab.com/oauth/authorize?${searchParam}`
  
    window.location = uri;
  }
  render() {
    return (
      <div className="card">
        <Card myJSON={this.state.myJSON} loading={this.state.loading}/>
      </div>
    );
  }
}

export default withRouter(App)
