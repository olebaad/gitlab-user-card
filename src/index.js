/**
 * Dependencies
 * @ignore
 */
import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'
import App from './App'

/**
 * Module Dependencies
 * @ignore
 */
import './index.css'
import * as serviceWorker from './serviceWorker'

/**
 * Mount
 * @ignore
 */
ReactDOM.render(
  <Router>
    <App />
  </Router>, document.getElementById('root'))

/**
 * Deregister Service Worker
 * @ignore
 */
serviceWorker.unregister()